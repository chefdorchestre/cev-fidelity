<?php

namespace App\Entity;
use App\Entity\Quantity;
use App\Entity\User;

use App\Repository\CommandeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRepository::class)
 */
class Commande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=user::class, inversedBy="commandes")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Quantity::class, mappedBy="commande", cascade={"persist"})
     */
    private $quantities;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fidelityPoints;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fidelityAmount;

    public function __construct()
    {
        $this->quantities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Quantity[]
     */
    public function getQuantities(): Collection
    {
        return $this->quantities;
    }

    public function addQuantity(Quantity $quantity): self
    {
        if (!$this->quantities->contains($quantity)) {
            $this->quantities[] = $quantity;
            $quantity->setCommande($this);
        }

        return $this;
    }

    public function removeQuantity(Quantity $quantity): self
    {
        if ($this->quantities->removeElement($quantity)) {
            // set the owning side to null (unless already changed)
            if ($quantity->getCommande() === $this) {
                $quantity->setCommande(null);
            }
        }

        return $this;
    }

    public function getFidelityPoints(): ?int
    {
        return $this->fidelityPoints;
    }

    public function setFidelityPoints(?int $fidelityPoints): self
    {
        $this->fidelityPoints = $fidelityPoints;

        return $this;
    }

    public function getFidelityAmount(): ?float
    {
        return $this->fidelityAmount;
    }

    public function setFidelityAmount(?float $fidelityAmount): self
    {
        $this->fidelityAmount = $fidelityAmount;

        return $this;
    }
}
