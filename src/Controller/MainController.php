<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserType;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Repository\QuantityRepository;
use App\Repository\CommandeRepository;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\Quantity;
use App\Entity\Commande;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use DateTimeImmutable;

class MainController extends AbstractController
{
    private $session;

   public function __construct(SessionInterface $session)
   {
       $this->session = $session;
   }

  /**
   * @Route("/fidelity", name="fidelity")
   */
    public function fidelity(CommandeRepository $commandeRepo, UserRepository $userRepo, Request $req): Response
    {
        $aFidelities = [];
        $iUserId = $req->get('user');
      if($iUserId){
        $aPerriods = [
          ['start' => '2021-01-01', 'end' => '2021-04-30'],
          ['start' => '2021-05-01', 'end' => '2021-08-31'],
          ['start' => '2021-10-01', 'end' => '2021-12-30']
        ];

        foreach ($aPerriods as $aPeriod) {
          $aFidelities[] = $commandeRepo->findByPeriode($aPeriod, $iUserId);
        }
      }
        return $this->render('main/fidelity.html.twig', [
          'users' =>  $userRepo->findAll(),
          'fidelities' => $aFidelities
        ]);
    }
    /**
    * @Route("/buy", name="buy")
    */
    public function buy(ProductRepository $productRepo, UserRepository $userRepo, QuantityRepository $qteRepo, Request $req): Response
    {
        $em = $this->getDoctrine()->getManager();
        $aAllProducts = $productRepo->findAll();
        $aProduits = $req->request->all();
        // get current user from session
        $id = $this->session->get('user_id');
        $oCurrentUser = $userRepo->find($id);

        // add the four prodcut

        /*for($i=1; $i<=4; $i++){
           $oProduct = new Product();
           $oProduct->setName("P".$i);
           $em->persist($oProduct);
           $em->flush();
          }*/

        if($this->isNotEmptyValues($aProduits)){

          $oCommande = new Commande();
          $oCommande->setCreatedAt(new DateTimeImmutable());
          $oCommande->setUser($oCurrentUser);

          foreach($aProduits as $key => $iQte){
            if($iQte > 0){
              $oQte = new Quantity();
              $oQte->setCommande($oCommande);
              $oQte->setProduct($productRepo->find($key));
              $oQte->setQte($iQte);
              $em->persist($oQte);
              $em->flush();
            }
          }
          $this->fidelize($oCommande, $qteRepo);
          $em->persist($oCommande);
          $em->flush();
        }

        return $this->render('main/buy.html.twig', [
          'products' => $aAllProducts,
        ]);
    }

  /**
   * @Route("/user", name="user")
   */
    public function user(Request $req): Response
    {
        $em = $this->getDoctrine()->getManager();
        $oUser = new User();
        $form = $this->createForm(UserType::class, $oUser);
        $form->handleRequest($req);

        if($form->isSubmitted())
        {
          $em->persist($oUser);
          $em->flush();
          // start session
          $this->session->set('user_id', $oUser->getId());
          $this->session->set('user_name', $oUser->getName());
        }
        return $this->render('main/user.html.twig', [
          'form' => $form->createView()
        ]
      );
    }

    public function isNotEmptyValues($arr)
    {
      foreach ($arr as $value){
        if("" != $value){
          return true;
        }
      }
    }
    // TODO : resolve persistance issue
    public function fidelize($oCommande, $qteRepo)
    {
        $em = $this->getDoctrine()->getManager();
        $aQte = $oCommande->getQuantities();
        $aQtes = $qteRepo->findBy(['commande' => $oCommande]);

        $iCountPoints[] = 0;

        foreach($aQtes as $key => $oQte){
          $sProductName= $oQte->getProduct()->getName();

          $iQte = $oQte->getQte();

            if($sProductName == "P1"){
              $iCountPoints[$iQte] = 5*$oQte->getQte();
              if($sProductName == "P2"){
                $iCountPoints[$iQte]=5;
              }
            }
            elseif($sProductName == "P3" and $iQte>2){
              $iCountPoints[$iQte]=15;
            }elseif($sProductName == "P4"){
              $iCountPoints[$iQte]=35*$iQte;
            }
          }
          $iPoints = array_sum($iCountPoints);
          $oCommande->setFidelityPoints($iPoints);
          $oCommande->setFidelityAmount($iPoints*0.01);
    }
}
